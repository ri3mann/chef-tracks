#
# Cookbook Name:: tracks
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

%w(build-essential liblzma-dev zlib1g-dev libmysqlclient-dev). each do |pkg|
  package pkg
end

poise_service_user node['tracks']['service_user'] do
  group node['tracks']['service_group']
  home node['tracks']['home']
end

application node['tracks']['home'] do
  git 'https://github.com/TracksApp/tracks.git'
  bundle_install do
    deployment true
    without %w(development test sqlite)
  end
  rails do
    database({
      adapter: 'mysql2',
      username: node['tracks']['mysql']['user'],
      password: node['tracks']['mysql']['password'],
      host: node['tracks']['mysql']['host'],
      database: node['tracks']['mysql']['db'],
    })
    precompile_assets true
    migrate true
  end
  owner node['tracks']['service_user']
end

poise_service 'tracks' do
  command '/opt/chef/embedded/bin/bundle exec rails server'
  user node['tracks']['service_user']
  directory node['tracks']['home']
  environment ({
    RAILS_ENV: 'production',
    HOME: node['tracks']['home']
  })
end
