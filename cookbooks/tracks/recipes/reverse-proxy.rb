#
# Cookbook Name:: tracks
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

nginx_proxy node['tracks']['domainname'] do
  url 'http://localhost:3000/'
end
