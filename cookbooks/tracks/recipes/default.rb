#
# Cookbook Name:: tracks
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

include_recipe 'apt'
include_recipe '::database'
include_recipe '::rails-app'
include_recipe '::reverse-proxy'
