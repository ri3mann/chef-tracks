# Cookbook Name:: tracks
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

package 'libmysqlclient-dev'
gem_package 'mysql2'

mysql_service 'default' do
  version '5.5'
  data_dir '/data'
  initial_root_password node['tracks']['mysql']['initial_root_password']
  action [:create, :start]
end

mysql_connection_info = {
  :host     => node['tracks']['mysql']['host'],
  :username => 'root',
  :password => node['tracks']['mysql']['initial_root_password']
}

mysql_database_user node['tracks']['mysql']['user'] do
  connection mysql_connection_info
  database_name node['tracks']['mysql']['db']
  password      node['tracks']['mysql']['password']
  action        :grant
end

mysql_database 'tracks' do
  connection(
    :host     => '127.0.0.1',
    :username => 'root',
    :password => node['tracks']['mysql']['initial_root_password']
  )
  action :create
end
