class MyResource < PoiseApplicationRuby::Resources::Rails::Resource

  include PoiseApplicationRuby::AppMixin
  provides(:application_rails)

  attribute(:site_config, template: true, default_source: 'site.yml.erb')
  #, default_options: lazy { default_site_options })

end

class MyProvider < PoiseApplicationRuby::Resources::Rails::Provider

  include PoiseApplicationRuby::AppMixin
  provides(:application_rails)

  def write_site_yml
    file ::File.join(new_resource.path, 'config', 'site.yml') do
      user new_resource.parent.owner
      group new_resource.parent.group
      mode '640'
      content new_resource.site_config_content
    end
  end

  def action_deploy
    set_state
    notifying_block do
      write_database_yml unless new_resource.database.empty?
      write_secrets_config if new_resource.secret_token
      write_site_yml
      precompile_assets if new_resource.precompile_assets
      run_migrations if new_resource.migrate
    end
  end
end
