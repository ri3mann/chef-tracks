Description:
===========
The cookbook is minimalistic, meant only to demonstrate my approach, and is rather rough on edges, quickly put together, not thoroughly tested or thinked over. 
My preference is using ready components with minimal configuration, and try not to originate inhouse solutions.
At the same time readability shouldn't be impacted, the components should be well maintained, adopted and supported.

Improvements:
============
Real life production set up would use roles and environments, as well as cookbook versions pinned per environment.
Also secrets would be coming from Vault or Chef-Vault or at least encrypted data bag.
The extention made to Poise Application cookbook resource would probably be good for pull request.
